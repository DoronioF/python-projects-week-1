# created a function for the lines of code which cover converting
# plain text into emojis
# that way it's easier to understand what the code does
def emoji_generator(message):
    words = message.split(" ")
    emojis = {
        ":)" : "😀",
        ":(" : "😞"
    }
    output = ""
    for word in words:
        output += emojis.get(word, word) + " "
    return output

message = input(">")
print(emoji_generator(message))
