# 1. Convert two lists into a dictionary
num_dict = {}

keys = ['ten', 'twenty', 'thirty']
values = [10, 20, 30]

print('1.')
numb_dict = dict(zip(keys, values))
print(numb_dict)

print('\n---')

for key in keys:
    for value in values:
        num_dict[key] = value
        values.remove(value)
        break

print(num_dict, '\n')

# expected output
# {
#     'ten': 10,
#     'twenty': 20,
#     'thirty': 30
# }

# 2. Delete a list of keys from a dictionary

person = {
    'name': 'Jessica',
    'age': 20,
    'transportation': 'teleport',
    'city': 'Oklahoma'
}

remove_keys = ['age', 'city']

for key in remove_keys:
    person.pop(key)

print('2.')
print(person, '\n')

# 3. rename transportation to 'super power'
print('3.')
person['super power'] = person.pop('transportation')
print(person, '\n')


# 4. take two dictionaries and merge into one dictionary

dict1 = {
    'ten': 10,
    'twenty': 20,
    'thirty': 30
}

dict2 = {
    'forty': 40,
    'fifty': 50,
    'sixty': 60
}

dict3 = dict1 | dict2

print('4.')
print(dict3)
