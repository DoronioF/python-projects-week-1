from operator import truediv
from pickle import TRUE


age_string = input("How old are you?")
age = int(age_string)
print("Next year, you'll be", age + 1, "years old")
