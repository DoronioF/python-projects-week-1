from pickle import FALSE, TRUE


age = 19

if age > 12 and age < 20:
    is_teenager = TRUE
else:
    is_teenager = FALSE

if is_teenager:
    print("Are you on TikTok?")
else:
    print("Are you on Facebook?")