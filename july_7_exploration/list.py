# nums = [1, 2, 3, 5, 7, 10, 25, 20, 100, 15]

# def multiples_of_5(any_list):
#     is_multiple_of_5 = []
#     for num in any_list:
#         if num % 5 == 0:
#             is_multiple_of_5.append(num)
#     return is_multiple_of_5


# print(multiples_of_5(nums))

fruits = [
    "apple", 
    "banana", 
    "orange", 
    "apricot", 
    "kiwi", 
    "grape"
]

cars = [
    "bmw", 
    "honda", 
    "toyota", 
    "subaru", 
    "ferari"
    ]

def word_updater(anything):
    current_word = ""
    altered_words = []
    for word in anything:
        altered_words.append(word.capitalize()[::-1].lower())
    return altered_words

print(word_updater(fruits))
print(word_updater(cars))