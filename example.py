#birth_year = int(input("What year were you born in? " ))

#current_year = 2022

#age = current_year - birth_year

#print(f"You are {age} years old")

#Control Flow

#color = input('Enter "green", "yellow", "red": ' ).lower()
#print(f'The user entered {color}')

input_limit = 99
input_count = 0

while input_count < input_limit:
    user_input = input('Enter "green", "yellow", "red", "exit": ' ).lower()
    print(f'The user entered {user_input}')
    input_count += 1
    if user_input == 'green':
        print('Go!')
    elif user_input == 'yellow':
        print('Slow Down!')
    elif user_input == 'red':
        print('Stop!')
    elif user_input == 'exit':
        print('user has exited')
        break
    else:
        print('Bogus!')

# Below that code, write an `if...elif...else` statement that prints out one of the following messages:

# - If `green` is entered, print the message `Go!`
# - If `yellow` is entered, print the message `Slow Down!`
# - If `red` is entered, print the message `Stop!`
# - If anything else is entered, print the message `Bogus!`




